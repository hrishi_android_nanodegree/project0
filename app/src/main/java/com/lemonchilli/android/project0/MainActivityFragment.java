package com.lemonchilli.android.project0;

import android.content.res.XmlResourceParser;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {


    ArrayList<MyProject> projects;

    public MainActivityFragment() {
        projects=new ArrayList<MyProject>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);




        XmlResourceParser parser=getContext().getResources().getXml(R.xml.myprojects);

        projects.clear();

        int eventType= XmlPullParser.START_DOCUMENT;
        while(eventType!=XmlPullParser.END_DOCUMENT){
            try {
                String projectName;
                String projectAppId;
                boolean isSpecial;


                parser.next();
                eventType=parser.getEventType();



                if(eventType==XmlPullParser.START_TAG && parser.getName().equalsIgnoreCase("project")){
                    projectAppId=parser.getAttributeValue(null, "appid");
                    isSpecial=parser.getAttributeBooleanValue(null, "special", false);
                    projectName=parser.nextText();

                    projects.add(new MyProject(projectName,projectAppId,isSpecial));
                }

            } catch (XmlPullParserException | IOException e) {
                e.printStackTrace();
            }
        }

        ListView listView=(ListView) rootView.findViewById(R.id.listview_projects);
        MyProjectAdapter customAdapter=new MyProjectAdapter(getContext(),projects);
        listView.setAdapter(customAdapter);



        return rootView;
    }
}
