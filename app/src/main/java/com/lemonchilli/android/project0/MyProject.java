package com.lemonchilli.android.project0;

/**
 * Created by Hrishi on 21/09/2015.
 */
public class MyProject {

    public String projectName="";
    public String projectAppId="";
    public boolean isSpecial=false;

    public MyProject(String name, String appId,boolean special){
        this.projectName=name;
        this.projectAppId=appId;
        this.isSpecial=special;
    }
}
