package com.lemonchilli.android.project0;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Hrishi on 21/09/2015.
 */
public class MyProjectAdapter extends BaseAdapter {


    private ArrayList<MyProject> items;
    private LayoutInflater inflater;

    public MyProjectAdapter(Context context,ArrayList<MyProject> data){

        inflater=LayoutInflater.from(context);
        this.items=data;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(R.layout.list_item_projectlink, null);
        }
        Button btn=(Button) convertView.findViewById(R.id.list_item_projectlink_button);
        btn.setText(items.get(position).projectName);
        btn.setTag(position);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyProject project=items.get((int)v.getTag());

                if(project!=null){
                    if(project.projectAppId==null){
                        Toast toast = Toast.makeText(inflater.getContext(), "This button will launch the "+project.projectName+" app in the future!", Toast.LENGTH_LONG);
                        toast.show();
                    }else{
                        try {
                            Intent launchIntent = inflater.getContext().getPackageManager().getLaunchIntentForPackage("com.package.address");
                            inflater.getContext().startActivity(launchIntent);
                        }catch(Exception ex) {
                            Toast toast = Toast.makeText(inflater.getContext(), "Failed to launch app: "+ex.getMessage(), Toast.LENGTH_LONG);
                            toast.show();
                        }

                    }
                }

            }
        });

        if(items.get(position).isSpecial){
            btn.setBackgroundColor(0xFFD80043);
        }else{
            btn.setBackgroundColor(0xFFF08C35);
        }

        return convertView;
    }
}
